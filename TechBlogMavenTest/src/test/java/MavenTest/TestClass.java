package MavenTest;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import junit.framework.Assert;
import java.util.Iterator;
import org.junit.Before;
import org.junit.Test;
import java.util.List;
import org.junit.After;

public class TestClass {
	private WebDriver driver;

	@Before
	public void setUp() throws Exception {
		System.setProperty("webdriver.gecko.driver", "/Users/olusarioz/Desktop/geckodriver");
		driver = new FirefoxDriver();
	}

	@Test
	public void verify() {
		driver.get("http://techcrunch.com");
		//latest news list
		WebElement latestNewsList = driver.findElement(By.cssSelector("div[class='river river--homepage ']"));
		//list all the news within this list
		List<WebElement> news = latestNewsList.findElements(By.tagName("article"));
		//System.out.println(news.size());
		for (int i = 0; i < news.size(); i++) {
			//get author for each news
			String author = news.get(i).findElement(By.cssSelector("span.river-byline__authors")).getText();
			//System.out.println(author);
			//get image for each news
			String imgUrl = news.get(i).findElement(By.cssSelector("figure.post-block__media img")).getAttribute("src");
			//System.out.println(imgUrl);
			Assert.assertTrue("This news has an author", author != null);
			Assert.assertTrue("This news has an image", imgUrl != null);
		}

		
		driver.get("http://techcrunch.com");
		WebElement latestBlock = driver.findElement(By.cssSelector("div[class='river river--homepage ']"));
		List<WebElement> news2 = latestBlock.findElements(By.tagName("article"));
		System.out.println(news2.size());
		news2.get(0).click();
		System.out.println(driver.getTitle());
		String browserTitle=driver.getTitle();
		latestBlock = driver.findElement(By.cssSelector("div[class='river river--homepage ']"));
		// news are listed for this block
		news2 = latestBlock.findElements(By.tagName("article"));
		System.out.println(news2.get(0).findElement(By.cssSelector("h1.article__title")).getText());
		String newsTitle=news2.get(0).findElement(By.cssSelector("h1.article__title")).getText();
		
		//WebElement links = news.get(0).findElement(By.tagName("a"));
		//System.out.println(links);
		//List<WebElement> url = links.findElements(By.);
		java.util.List<WebElement> links = news2.get(0).findElements(By.tagName("a"));
		//System.out.println(links);
		System.out.println("Number of links within the content is "+links.size());
		Assert.assertEquals(browserTitle, newsTitle);
	}

	@After
	public void tearDown() throws Exception {
		Thread.sleep(5000);
		driver.quit();
	}

}
