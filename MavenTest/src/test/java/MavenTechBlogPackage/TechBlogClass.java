package MavenTechBlogPackage;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.Test;

import junit.framework.Assert;



public class TechBlogClass {
	private WebDriver driver;

	@Test
	public void setUp() throws Exception {
		System.setProperty("webdriver.gecko.driver", "/Users/olusarioz/Desktop/geckodriver");
		driver = new FirefoxDriver();
	}
	
	@Test
	public void verifyAuthorImage() {
		driver.get("http://techcrunch.com");
		//latest news list
		WebElement latestNewsList = driver.findElement(By.cssSelector("div[class='river river--homepage ']"));
		List<WebElement> news = latestNewsList.findElements(By.tagName("article"));
		System.out.println(news.size());
		for (int i = 0; i < news.size(); i++) {
			//get author for each news
			String author = news.get(i).findElement(By.cssSelector("span.river-byline__authors")).getText();
			//System.out.println(author);
			//get image for each news
			String imgUrl = news.get(i).findElement(By.cssSelector("figure.post-block__media img")).getAttribute("src");
			//System.out.println(imgUrl);
			Assert.assertTrue("This news has an author", author != null);
			Assert.assertTrue("This news has an image", imgUrl != null);
		}

	}

	@Test
	public void verifyTitleLinks() {
		driver.get("http://techcrunch.com");
		WebElement latestBlock = driver.findElement(By.cssSelector("div[class='river river--homepage ']"));
		List<WebElement> news = latestBlock.findElements(By.tagName("article"));
		System.out.println(news.size());
		news.get(0).click();
		System.out.println(driver.getTitle());
		String browserTitle=driver.getTitle();
		latestBlock = driver.findElement(By.cssSelector("div[class='river river--homepage ']"));
		// news are listed in this block
		news = latestBlock.findElements(By.tagName("article"));
		System.out.println(news.get(0).findElement(By.cssSelector("h1.article__title")).getText());
		String newsTitle=news.get(0).findElement(By.cssSelector("h1.article__title")).getText();
		
		//WebElement links = news.get(0).findElement(By.tagName("a"));
		//System.out.println(links);
		//List<WebElement> url = links.findElements(By.);
		java.util.List<WebElement> links = news.get(0).findElements(By.tagName("a"));
		//System.out.println(links);
		System.out.println("Number of links within the content is "+links.size());
		Assert.assertEquals(browserTitle, newsTitle);
	}



}


	
	
